﻿using System;
using System.Net;
using System.Threading.Tasks;
using Flexo.Blogging.Authors;
using Flexo.Blogging.Blogs;
using Flexo.Blogging.Posts;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Http.Client;

namespace DotnetSDKSample;

public class BloggingDemoService : ITransientDependency
{
    private readonly IBlogAppService _blogAppService;
    private readonly IPostAppService _postAppService;
    private readonly IAuthorAppService _authorAppService;

    public BloggingDemoService(
        IBlogAppService blogAppService,
        IPostAppService postAppService,
        IAuthorAppService authorAppService)
    {
        _blogAppService = blogAppService;
        _postAppService = postAppService;
        _authorAppService = authorAppService;
    }

    public async Task RunAsync()
    {
        try
        {
            //var blog = await _blogAppService.GetBySlugAsync("marketing");
            //if (blog == null)
            //    return;

            //Console.WriteLine($"Blog --------------------------------");
            //Console.WriteLine($"Blog name         : {blog.Name}");
            //Console.WriteLine($"Slug             : {blog.Slug}");

            var post = await _postAppService.GetBySlugAsync(/*blog.Slug*/"marketing", "instagram-video-formats");
            if (post == null)
                return;

            Console.WriteLine($"--------------------------------------");
            Console.WriteLine($"Post --------------------------------");
            Console.WriteLine($"Blog name         : {post.BlogName}");
            Console.WriteLine($"Title             : {post.Title}");
            Console.WriteLine($"Short description : {post.ShortDescription}");
            Console.WriteLine($"Author name       : {post.Author?.Name}");

            if (post.Author == null)
                return;

            var author = await _authorAppService.GetAsync(post.Author.Id);
            if (author == null)
                return;

            Console.WriteLine($"--------------------------------------");
            Console.WriteLine($"Author --------------------------------");
            Console.WriteLine($"Author name         : {author.Name}");
            Console.WriteLine($"Email             : {author.Email}");
            Console.WriteLine($"Description : {author.Description}");
        }
        catch (AbpRemoteCallException e)
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.Red;
            if (e.HttpStatusCode == (int)HttpStatusCode.NotFound)
            {
                //Not found
                Console.WriteLine("Not found ! : " + e.Message);
            }
            else
            {
                Console.WriteLine(e.Error.Message);
            }
        }
        
    }
}
